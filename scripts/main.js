student = {
    name: "",
    lastName: "",
}

student.table = {};

student.name = prompt("Введіть ім'я студента");
while (!student.name) {
    student.name = prompt("Введіть ім'я студента");
}
student.lastName = prompt("Введіть прізвище студента");
while (!student.lastName) {
    student.lastName = prompt("Введіть прізвище студента");
}

console.log(student);

let subject = prompt("Введіть предмет студента");
let mark = +prompt("Введіть оцінку за цей предмет");

while (subject && mark) {
    student.table[subject] = mark;
    subject = prompt("Введіть предмет");
    if (subject == null) break;
    mark = +prompt("Введіть оцінку за цей предмет");
    if (mark == null) break;
}

let i = 0;
let calc = 0;
let sumMark = 0;

for (let key in student.table) {
    sumMark += student.table[key];
    if (student.table[key] < 4) {
        i++;
    }
    calc++;
}

console.log(`Кількість поганих оцінок з предметів" ${i}.`);

if (i === 0) alert("Студента переведено на наступний курс");

console.log(`Cередній бал з предметів" ${Math.floor(sumMark / calc)}.`);

if ((sumMark / calc) > 7) alert("Студенту призначено стипендію");


